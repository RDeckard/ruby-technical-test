# frozen_string_literal: true

require_relative '../lib/twin_words/extend_service'
require_relative '../lib/twin_words/fast_service'

[
  TwinWords::ExtendService,
  TwinWords::FastService
].each do |class_service|
  describe class_service do
    before(:each) do
      @service = class_service.new('fakepath.txt')
      @service.words = %w[al bums albums we table aver weaver tail word or tailor]
    end

    context '#words_by_length' do
      it 'should return a sorted hash of array of words' \
         ' (by length then by alphabetical order)' do
        @service.words_by_length
        expect(@service.words_by_length).to eq(
          2 => %w[al or we],
          4 => %w[aver bums tail word],
          5 => %w[table],
          6 => %w[albums tailor weaver]
        )
      end
    end

    context '#call(6)' do
      it 'should return 6 words length composed by shorter words' do
        expect(@service.call(6)).to eq(%w[albums tailor weaver])
      end
    end

    context '#results' do
      it 'should return an empty hash before #call' do
        expect(@service.results).to eq({})
      end

      it 'should return a hash with all results after successive #call' do
        @service.call(6)
        @service.call(4)
        expect(@service.results).to eq(
          6 => %w[albums tailor weaver],
          4 => []
        )
      end
    end
  end
end
