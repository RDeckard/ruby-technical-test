# frozen_string_literal: true

require 'benchmark'
require_relative 'twin_words/extend_service'
require_relative 'twin_words/fast_service'

[
  TwinWords::ExtendService,
  TwinWords::FastService
].each do |class_service|
  service = nil

  puts "Benchmark #{class_service}:"
  Benchmark.bm do |x|
    x.report('init: ') do
      service = class_service.new('wordlist.txt')
    end
    x.report('warming: ') do
      service.words_by_length
    end
    (4..9).each do |target_words_length|
      x.report("target length #{target_words_length}: ") do
        service.call(target_words_length)
      end
    end
  end

  puts(service.results.map { |k, v| "target length #{k} => #{v.count} words" })
  puts 'Result for word length of 6:'
  puts service.results[6].join(', ')
  puts
end
