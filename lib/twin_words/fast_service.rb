# frozen_string_literal: true

require 'parallel'
require 'etc'
require_relative 'gen_service'

module StringRefinement
  refine String do
    def first(n = 1)
      self[0, n]
    end

    def last(n = 1)
      self[-n, n]
    end
  end
end

using StringRefinement

module TwinWords
  class FastService < GenService
    private

    def strategy_by_length(length)
      twin_length = target_word_length - length
      words_by_length[target_word_length].select do |target_word|
        words_by_length[length]&.bsearch { |w| target_word.first(length) <=> w } &&
          words_by_length[twin_length]&.bsearch { |w| target_word.last(twin_length) <=> w }
      end
    end
  end
end
