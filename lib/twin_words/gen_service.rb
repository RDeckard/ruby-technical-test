# frozen_string_literal: true

require 'parallel'
require 'etc'

module TwinWords
  class GenService
    attr_writer :words
    attr_reader :words_list_path
    attr_reader :target_word_length
    attr_reader :results

    def initialize(words_list_path)
      @words_list_path = words_list_path
      @results = new_hash_of_arrays
    end

    def call(target_word_length)
      @target_word_length = target_word_length
      if results[target_word_length].empty?
        @results[target_word_length] = perform
      else
        results[target_word_length]
      end
    end

    # Hash of words sorted by length
    def words_by_length
      @words_by_length ||=
        words
        .grep(/\w+/)
        .map { |w| w.strip.downcase }
        .sort.uniq
        .each_with_object(new_hash_of_arrays) do |word, acc|
          acc[word.length] << word
        end
        .sort.to_h
    end

    # Raw array of words from file
    def words
      @words ||=
        File.readlines(words_list_path)
    end

    private

    # Dispatch work by word length on all available processors
    def perform
      return [] unless words_by_length[target_word_length]

      Parallel.map(
        relevant_word_lengths,
        in_processes: nprocesses
      ) do |length|
        strategy_by_length(length)
      end.flatten.compact.uniq.sort
    end

    # The strategy to adopt, by word length
    def strategy_by_length(_)
      raise '#strategy_by_length must be define in a subclass'
    end

    # Some word lengths are irrelevant (e.g. 5 for target length of 6, when there's no 1 letter words)
    def relevant_word_lengths
      (min_word_length...target_word_length).to_a -
        (min_word_length - 1).downto(1).map { |lgth| target_word_length - lgth }
    end

    def min_word_length
      words_by_length.keys.min
    end

    def nprocesses
      needed_processes = target_word_length - min_word_length + 1
      [needed_processes, Etc.nprocessors].min
    end

    def new_hash_of_arrays
      Hash.new { |hash, key| hash[key] = [] }
    end
  end
end
