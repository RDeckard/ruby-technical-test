# frozen_string_literal: true

require 'parallel'
require 'etc'
require_relative 'gen_service'

module TwinWords
  class ExtendService < GenService
    attr_reader :details

    # details parameter is used in this class' strategy
    def initialize(words_list_path, details: false)
      @details = details
      super(words_list_path)
    end

    private

    # This strategy, with details attribut set on `true`
    # give us details on how result's words are composed.
    def strategy_by_length(length)
      twin_length = target_word_length - length
      words_by_length[length]&.map do |first_word|
        relevant_target_words =
          words_by_length[target_word_length].grep(/^#{first_word}/)
        words_by_length[twin_length]&.map do |last_word|
          tested_word = first_word + last_word
          if relevant_target_words.bsearch { |w| tested_word <=> w }
            details ? "#{tested_word} (#{first_word} + #{last_word})" : tested_word
          end
        end
      end
    end
  end
end
