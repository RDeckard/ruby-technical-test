#### Technical Test - Backend Developer
# Dictionary Challenge

The challenge here is to process the dictionary (in attachement) and looking for all six letter words which are composed of two concatenated smaller words.

For example:
```
al + bums => albums
we + aver => weaver
tail + or => tailor
[...]
```

## Tasks

There are 3 different parts to this exercise: writing three different programs to solve the same problem, but each time with a different objective in mind: Readability, Speed and finally Extensibility.

Addressing only the first two programs will be sufficient this time (Readability and Speed).

If you want to include a brief explanation of the differences, the explanation can actually be more interesting than the Ruby code itself.  Attached is a file that goes with the exercise: a dictionary or word list.

Additional instructions:

- Some unit tests (RSpec/Cucumber/Test-unit….) will be a plus but are not mandatory. Testing is part of our daily routine... 
- Object oriented design and small functions with a meaningful name have a lot of value for us.
- We don't hesitate to challenge each other code.
